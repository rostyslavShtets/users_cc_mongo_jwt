package main

import (
	"users_cc_mongo_jwt/api"
	"users_cc_mongo_jwt/utils"
	"users_cc_mongo_jwt/middleware"
	"github.com/gorilla/mux"
	"fmt"
	"log"
	"net/http"
)
const port = ":8081"

func main() {
	// additional internal check 
	utils.Hello()
	fmt.Println("new_new")

	// init DB
	api.InitDB()
			
	r := mux.NewRouter()
	// r.Path("/").HandlerFunc(homeHandler)

	r.HandleFunc("/", api.HomePage)
	r.HandleFunc("/login", api.LoginPage)
	r.HandleFunc("/logout", api.Logout).Methods("GET")
	r.HandleFunc("/signup", api.SignUpPage)
	
	//request for people

	r.HandleFunc("/people", middleware.AuthenticationMiddleware(api.AllPeopleEndPoint)).Methods("GET")
	r.HandleFunc("/people/{personID}", middleware.AuthenticationMiddleware(api.FindPersonEndPoint)).Methods("GET")
	r.HandleFunc("/people", middleware.AuthenticationMiddleware(api.CreatePersonEndPoint)).Methods("POST")
	r.HandleFunc("/people/{personID}", middleware.AuthenticationMiddleware(api.UpdatePersonEndPoint)).Methods("PUT")
	r.HandleFunc("/people/{personID}", middleware.AuthenticationMiddleware(api.DeletePersonEndPoint)).Methods("DELETE")
	
	//filters
	r.HandleFunc("/filter/people", api.GetPeopleFilter).Methods("GET")
	r.HandleFunc("/filter_object/people", api.GetPeopleFilterObject).Methods("GET")
		

	

	//request for users
	r.HandleFunc("/users", middleware.AuthenticationMiddleware(api.AllUsersEndPoint)).Methods("GET")



	fmt.Printf("Serv on port %v....\n", port)
	if err := http.ListenAndServe(port, r); err != nil {
		log.Fatal(err)
	}
	// log.Fatal(http.ListenAndServe(port, r))
	// r.HandleFunc("/registration", api.CreateUserEndPoint).Methods("POST")
		// r.HandleFunc("/users/email/{email}", api.FindUserByEmailEndPointGET).Methods("GET")
}


