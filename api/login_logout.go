package api

import (
	"fmt"
	"net/http"
	"log"
	"github.com/dgrijalva/jwt-go"
	"users_cc_mongo_jwt/models"
	"encoding/json"
	// "io/ioutil"
)

func CheckIfUserIsAuthenticated(w http.ResponseWriter, r *http.Request) bool {
	return true
}

func LoginPage(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.ServeFile(w, r, "templates/login.html")
		return
	}

	//get user from put request
	userEmail := r.FormValue("userEmail")
	userPassword := r.FormValue("password")
	fmt.Println(userEmail)
	//get request to database
	userFromDB, err := FindUserByEmailEndPointPOST(userEmail)
	if err != nil {
		log.Println("can’t find user:", err)
		http.Redirect(w, r, "/login", 301)
		return
	}
	fmt.Println(userFromDB)

	//create and return token
	if (userEmail == userFromDB.Email && userPassword == userFromDB.Password) {

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"username": userFromDB.UserName,
			"password": userFromDB.Password,
		})
		tokenString, error := token.SignedString([]byte("secret"))
		if error != nil {
			fmt.Println(error)
		}
		json.NewEncoder(w).Encode(models.JwtToken{Token: tokenString})

		log.Println("You just have got token")
				
	} else {
		log.Println("password or user isn't correct", err)
		http.Error(w, "password or user isn't correct", http.StatusBadRequest)
		// w.Write([]byte("password or user isn't correct"))
	}
}

func Logout(w http.ResponseWriter, r *http.Request) {

}
