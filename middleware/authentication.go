package middleware

import (
	"net/http"
	"users_cc_mongo_jwt/models"
	"github.com/dgrijalva/jwt-go"
	"encoding/json"
	"fmt"
	"strings"
)
// func AuthenticationMiddleware(next http.HandlerFunc) http.HandlerFunc {
// 	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		check := api.CheckIfUserIsAuthenticated(w, r)
// 		fmt.Println(check)
//        	if check {
// 			next(w, r)
// 		} else {
// 			w.Write([]byte("<h1>you are not login<h1>"))
// 			return
// 		}
//     })
// }

func AuthenticationMiddleware(next http.HandlerFunc) http.HandlerFunc {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorizationHeader := r.Header.Get("authorization")
	    if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")
		    if len(bearerToken) == 2 {
			    token, error := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
                    if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
                        return nil, fmt.Errorf("There was an error")
                    }
                    return []byte("secret"), nil
                })
                if error != nil {
				    json.NewEncoder(w).Encode(models.Exception{Message: error.Error() + "_323"})
                    return
                }
                if token.Valid {
                   next(w, r)
                } else {
                    json.NewEncoder(w).Encode(models.Exception{Message: "Invalid authorization token"})
                }
            }
        } else {
            json.NewEncoder(w).Encode(models.Exception{Message: "An authorization header is required"})
        }
    })
}